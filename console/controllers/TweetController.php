<?php
namespace console\controllers;

use \Yii;
use yii\console\Controller;
use common\models\Common;
use Abraham\TwitterOAuth\TwitterOAuth;

require '/var/www/html/yiiconsole/twitter_api_yii_console/vendor/autoload.php';//local

class TweetController extends Controller {
   
    public function actionSavetodb()
    {
        $modelCommon = new Common();
        $filepath = '/var/www/html/yiiconsole/twitter_api_yii_console/';//local
        $file_err_save = $filepath.'/logs/err_save.txt';
	$array = '';
        $err_txt ="Errors occurred: ". PHP_EOL;
        
        
        try{//checks Twitter connection and data
            $connection = new TwitterOAuth();
            $content = $connection->get("account/verify_credentials");
            $json = $connection->get("search/tweets", ["q" => "#hastag1 OR #hastag2 OR #hastag3","count"=>100,"result_type"=>"mixed","since"=>date('Y-m-d', strtotime('-7 days'))]);
            if ($connection->getLastHttpCode() != 200) {
              $modelCommon->writeFile($file_err_save, json_encode(unserialize(serialize($connection))));
              die();
            }
            
            $json  = serialize($json);
            $json  = unserialize($json);
            $json  = json_encode($json);
            $array = json_decode($json, true);
            
            if(is_array($array) && !empty($array)){// runs only if the array is not empty
                $modelCommon->writeFile($filepath.'/data/data.json', $json);
                // TDOD
                //to store data into db
                
            }// end if "array not empty"
            else{//writes error logs
                echo 'Some errors occurred with Twitter request'. PHP_EOL;
                $modelCommon->writeFile($file_err_save, 'No JSON data');
            }
        }// end try "no json file"
        catch (\yii\base\ErrorException $e){// writes error logs
            echo 'Some errors occurred'. PHP_EOL;
            $modelCommon->writeFile($file_err_save, $e->getMessage());
        }
    }
}