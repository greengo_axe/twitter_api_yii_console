# Twitter, JSON, bash and Yii2 Console App

## Abstract 
To search data into Twitter and export them via JSON data through YII2 Console and bash

## Premise
This project requires :

* \>PHP 7.3
* Composer

They have to be up and running. 

## Installation
*  Download and install Yii2 framework(advanced template), via composer, on your local computer . For all info click [here](https://yiiframework.com) 
 *  The root folder of the project is 
 
       >**twitter_api_yii_console** 
    
 * and the absolute path is
 
      >**/var/www/html/yiiconsole/twitter_api_yii_console**
    
  * Clone the project from gitlab on Desktop :
    
    >**git@gitlab.com:greengo_axe/twitter_api_yii_console.git**

* copy and overwrite all folders and files from 

>>**Desktop/twitter_api_yii_console**

>into 

>>**/var/www/html/yiiconsole/twitter_api_yii_console**

* Delete the folder on Desktop (**Desktop/twitter_api_yii_console**)

* Change the permissions:
> **cd /var/www/html/yiiconsole/twitter_api_yii_console**
> 
> **chmod 777 yii**

* Insert Twitter parameters into:

 > /var/www/html/yiiconsole/twitter_api_yii_console/vendor/Abraham/TwitterOAuth/TwitterOAuth.php (To install it, just run "composer update" in the 'twitter_api_yii_console' folder)

* Enjoy!

## Troubleshooting
This project has been developed on Linux OS and there might be a few problems with the path or to grant the right permissions on Windows / Apple OSs.

* You're free to change the path of the project. If so, you have to replace the new path in the following files:

>1. console/controllers/**TweetController.php**

>1. console/scripts/**twitter_savetodb.sh**

* The direct commands of Yii2 Console are:

>> **/path/to/folder/twitter_api_yii_console/yii tweet/savetodb** 

>Ex.:

>>**/var/www/html/yiiconsole/twitter_api_yii_console/yii tweet/savetodb**